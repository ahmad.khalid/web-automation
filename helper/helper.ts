const getMonthName = (date) => {
    const monthlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    return monthlist[date.getMonth()];
}
const formatDate = (unformattedDate) => {
    const dateArr = unformattedDate.split('/');
    const [date, month, year] = dateArr;
    const formattedDate = new Date();
    formattedDate.setFullYear(year, month - 1, date);
    return formattedDate;
}

/**
 * this function converts amount in dollars string to number
 * @param amountInDollars
 */
const convertDollarToInt = (amountInDollars) : number => {
    return parseInt(amountInDollars.replace(/[^0-9.]+/g,""));
}

/**
 * compares two times given in xxh xxm format
 * @param timeInStr
 */

const getHourFromString = (timeInStr) => {
    return timeInStr.split(' ')[0].replace('h', '');
}

const getMinuteFromString = (timeInStr) => {
    return timeInStr.split(' ')[1].replace('m','');
}

const getShortestTime = (timeX, timeY): Boolean => {
    let firstTimeHour = getHourFromString(timeX)
    let firstTimeMinute = getMinuteFromString(timeX)
    let secondTimeHour = getHourFromString(timeY)
    let secondTimeMinute = getMinuteFromString(timeY);
    const firstTime = parseInt(firstTimeHour + firstTimeMinute);
    const secondTime = parseInt(secondTimeHour + secondTimeMinute);
    return firstTime < secondTime || firstTime === secondTime;
}

/**
 * this function calculates the difference in months between two dates
 * @param d1
 * @param d2
 */
const getMonthDiff = (d1: Date, d2: Date): number => {
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

export { convertDollarToInt, getShortestTime, formatDate, getMonthName, getMonthDiff}